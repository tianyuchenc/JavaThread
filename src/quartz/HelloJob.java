package quartz;

/**
 * @Auther: cty
 * @Date: 2020/1/9 6:38
 * @Description:  任务
 * @version: 1.0
 */
import java.util.Date;

import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

public class HelloJob implements Job {

    public HelloJob() {
    }

    public void execute(JobExecutionContext context)
            throws JobExecutionException {
        System.out.println("-----------start----------");
        // Say Hello to the World and display the date/time
        System.out.println("Hello World! - " + new Date());
        System.out.println("------------end----------");
    }
}
