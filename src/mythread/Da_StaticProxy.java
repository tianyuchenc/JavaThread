package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/4 22:14
 * @Description:  静态代理，通过婚庆公司代理婚庆理解
 * 公共接口：
 * 1、真实角色
 * 2、代理角色
 * @version: 1.0
 */
public class Da_StaticProxy {
    public static void main(String[] args) {
        new WeddingCompany(new BaJie()).happyMarry();
    }
}

//定义结婚接口
interface Marry{
    void happyMarry();
}

//定义结婚实体，八戒——真实角色
class BaJie implements Marry{
    @Override
    public void happyMarry() {
        System.out.println("八戒和嫦娥奔月了...");
    }
}

//定义婚庆公司实体——代理角色
class WeddingCompany implements Marry{
    //真实角色
    private Marry target;

    public WeddingCompany(Marry target){
        this.target = target;
    }

    private void ready(){
        System.out.println("布置猪窝...");
    }
    private void after(){
        System.out.println("闹玉兔...");
    }

    @Override
    public void happyMarry() {
        ready();  //准备工作
        this.target.happyMarry();
        after();  //事后工作
    }
}