package mythread;

import java.util.*;

/**
 * @Auther: cty
 * @Date: 2020/1/8 13:44
 * @Description:  任务定时调度  Timer和TimerTask类
 * @version: 1.0
 */
public class Ja_Other_TimerTest01 {
    public static void main(String[] args) {
        Timer timer = new Timer();

//        timer.schedule(new MyTask(),1000);  //倒计时1秒后执行任务一次
//        timer.schedule(new MyTask(),1000,2000);  //倒计时1秒后执行任务一次，之后每隔2秒执行一次
        Calendar cal = new GregorianCalendar(2020,0,8,14,16,00);  //注意0代表一月
//        Date date = new Date();  //当前时间
        timer.schedule(new MyTask(),cal.getTime(),2000);
    }
}

class MyTask extends TimerTask{
    @Override
    public void run() {
        for(int i=0; i<2; i++){
            System.out.println("放空大脑休息一会儿");
        }
        System.out.println("-----------end----------");
    }
}
