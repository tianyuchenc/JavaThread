package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/12 19:27
 * @Description:
 * @version: 1.0
 */

/**
 * 不可重入锁: 锁不可以延续使用
 *
 * @author 裴新 QQ:3401997271
 *
 */
public class Ji_Other_Lock02 {
    Lock lock = new Lock();
    public void a() throws InterruptedException {
        lock.lock();
        doSomething();
        lock.unlock();
    }
    //不可重入
    public void doSomething() throws InterruptedException {
        lock.lock();
        //...................
        lock.unlock();
    }
    public static void main(String[] args) throws InterruptedException {
        Ji_Other_Lock02 test = new Ji_Other_Lock02();
        test.a();  //此处锁死
        test.doSomething();
    }

}
// 不可重入锁
class Lock{
    //是否占用
    private boolean isLocked = false;
    //使用锁
    public synchronized void lock() throws InterruptedException {
        while(isLocked) {
            wait();
        }

        isLocked = true;
    }
    //释放锁
    public synchronized void unlock() {
        isLocked = false;
        notify();

    }
}
