package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 20:41
 * @Description: Thread中的其他方法：
 * 1、isAlive: 线程是否还活着
 * 2、Thread.currentThread(): 当前线程
 * 3、getName()：获取代理名称
 * 4、setName()：设置代理名称
 * @version: 1.0
 */
public class Gc_InfoTest {
    static class MyInfo implements Runnable{
        private String name;

        public MyInfo(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName()+"-->"+name);
        }
    }

    public static void main(String[] args) throws InterruptedException {
        System.out.println("当前线程是否还存在："+Thread.currentThread().isAlive());

        //设置名称：真实角色+代理角色
        MyInfo info = new MyInfo("战斗机");  //设置真实名称

//        Thread t = new Thread(info,"公鸡");  //设置代理名称方式1
        Thread t = new Thread(info);  //设置代理名称方式2
        t.setName("公鸡");

        t.start();
        Thread.sleep(1000);
        System.out.println("info线程是否还存在："+t.isAlive());
    }
}
