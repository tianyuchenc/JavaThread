package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 16:26
 * @Description: 终止线程
 * 1、线程正常执行完毕——>次数
 * 2、外部干涉——>加入标识位
 * 不要使用stop，不安全
 * @version: 1.0
 */
public class Fa_TerminatedFlag implements Runnable{
    //1、定义线程终止表示
    private boolean flag = true;
    private String name;
    private int i = 0;

    public Fa_TerminatedFlag(String name) {
        this.name = name;
    }

    @Override
    public void run() {
        //2、线程体与线程终止标识关联
        while(flag){
            System.out.println(name+"-->"+i++);
        }
    }

    //提供外部控制线程终止方法
    public void terminate(){
        flag = false;
    }

    public static void main(String[] args) {
        Fa_TerminatedFlag tt = new Fa_TerminatedFlag("C罗");
        new Thread(tt).start();

        for(int i=0; i<100000; i++){
            System.out.println("main-->"+i);
            if(i == 99999){
                tt.terminate();
            }
        }
    }
}
