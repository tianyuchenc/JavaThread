package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 15:52
 * @Description:
 * @version: 1.0
 */
public class Ec_LambdaRunnable02 {
    public static void main(String[] args) {
        new Thread(()->System.out.println("我一边学习Lambda")).start();
        new Thread(()->System.out.println("一边崩溃")).start();
        new Thread(()->{
            System.out.println("他一边听歌");
            System.out.println("一边玩游戏");
        }).start();
    }
}
