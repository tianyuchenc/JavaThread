package mythread;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: cty
 * @Date: 2020/1/7 13:07
 * @Description: 线程同步——块同步
 * @version: 1.0
 */
public class Hg_SynBlockTest2_2 {
    public static void main(String[] args) throws InterruptedException {
        List<String> list = new ArrayList<String>();

        for(int i=0; i<10000; i++){  //开启10000个线程
            new Thread(()->{
                synchronized (list){  //块同步，锁定list
                    list.add(Thread.currentThread().getName());
                }
            }).start();
        }

        Thread.sleep(10000);  //防止分线程还没运行完，主线程就结束
        System.out.println(list.size());
    }
}

/*
10000
*/