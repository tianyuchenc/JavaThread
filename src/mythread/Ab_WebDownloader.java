package mythread;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @Auther: cty
 * @Date: 2020/1/4 20:38
 * @Description: 从Web下载图片
 * @version: 1.0
 */

public class Ab_WebDownloader {
    public void download(String url, String name){
        try {
            FileUtils.copyURLToFile(new URL(url),new File(name));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

//    public static void main(String[] args) {
//        Ab_WebDownloader wd = new Ab_WebDownloader();
//        wd.download("https://dss2.bdstatic.com/6Ot1bjeh1BF3odCf/it/u=1745803631,611518551&fm=74&app=80&f=JPEG&size=f121,140?sec=1880279984&t=a9a2b43bb98f6839c2c81afad0ab4fc8","src/李小龙.png");
//    }
}
