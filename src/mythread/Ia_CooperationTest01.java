package mythread;

import java.util.concurrent.Callable;

/**
 * @Auther: cty
 * @Date: 2020/1/8 11:49
 * @Description:  并发协作模型（线程通信模型）“生产者消费者模式”——>管程法
 * 结构：生产者<——>缓冲区（商店）<——>消费者
 * 扩展：
 * pv：page view 网页的访问量/天
 * uv：unique view 用户的访问量/天
 * vv：visit view 网站的访问次数/天
 * @version: 1.0
 */
public class Ia_CooperationTest01 {
    public static void main(String[] args) {
        SynContainer synContainer = new SynContainer();
        Productor productor = new Productor(synContainer);
        Consumer consumer = new Consumer(synContainer);
        productor.start();
        consumer.start();
    }
}

//生产者（服务器）
class Productor extends Thread{
    SynContainer container;
    public Productor(SynContainer container){
        this.container = container;
    }

    @Override
    public void run() {
        //生产
        for(int i=0; i<100; i++){
            System.out.println("生产-->"+i+"个馒头");
            container.push(new Steamedbum(i));
        }
    }
}

//消费者（用户）
class Consumer extends Thread{
    SynContainer container;
    public Consumer(SynContainer container){
        this.container = container;
    }

    @Override
    public void run() {
        //消费
        for(int i=0; i<100; i++){
            System.out.println("消费-->"+container.pop().id+"个馒头");
        }
    }
}

//商店（缓冲区）
class SynContainer{
    Steamedbum[] buns = new Steamedbum[10];  //存储容器
    int count = 0;  //计数器
    //生产（存储）
    public synchronized void push(Steamedbum bun){
        //什么时候不能生产？——count==buns.length
        //不能生产，只能等待
        if(count==buns.length){
            try {
                this.wait();  //生产阻塞，消费者通知生产接触阻塞
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //存在空间，可以生产
        buns[count] = bun;
        count++;
        this.notify();  //通知消费者进行消费
    }
    //消费（获取）
    public synchronized Steamedbum pop() {
        //什么时候不能消费？——count==0
        //没有数据则等待
        if(count==0){
            try {
                this.wait();  //消费阻塞，生产者通知消费解除阻塞
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        //存在数据可以消费
        count--;
        Steamedbum bun = buns[count];
        this.notify();  //通知生产者进行生产
        return bun;
}
}

//馒头（资源）
class Steamedbum{
    int id;
    public Steamedbum(int i){
        this.id = i;
    }
}