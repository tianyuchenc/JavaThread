package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/4 20:58
 * @Description: 多线程下载图片
 * @version: 1.0
 */
public class Ac_ThreadDownloader extends Thread {
    private String url;
    private String name;

    private Ac_ThreadDownloader(String url, String name){
        this.url = url;
        this.name = name;
    }
    @Override
    public void run() {
        Ab_WebDownloader wd = new Ab_WebDownloader();
        wd.download(url, name);
        System.out.println(name);
    }

    public static void main(String[] args) {
        //创建线程
        Ac_ThreadDownloader td1 = new Ac_ThreadDownloader("https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2801390629,897502787&fm=26&gp=0.jpg","猪猪侠.png");
        Ac_ThreadDownloader td2 = new Ac_ThreadDownloader("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3462185629,2635822378&fm=26&gp=0.jpg","灰太狼.png");
        Ac_ThreadDownloader td3 = new Ac_ThreadDownloader("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1371386172,2124948373&fm=26&gp=0.jpg","蜘蛛侠.png");

        //启动线程
        td1.start();
        td2.start();
        td3.start();
    }
}
