package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/12 15:25
 * @Description:  ThreadLocal:每个线程自身的存储本地、局部区域
 * get/set/initialValue
 * @version: 1.0
 */
public class Jf_Other_ThreadLocal01 {
//    private static ThreadLocal<Integer> threadLocal = new ThreadLocal<>();

//    //更改初始化值 方式一
////    private static ThreadLocal<Integer> threadLocal = new ThreadLocal<>(){
////        protected Integer initialValue(){
////            return 200;
////        }
////    };

    //更改初始化值 方式二
    private static ThreadLocal<Integer> threadLocal = ThreadLocal.withInitial(()->100);

    public static void main(String[] args) {
        //获取值
        System.out.println(Thread.currentThread().getName()+"-->"+threadLocal.get());
        //设置值
        threadLocal.set(99);
        System.out.println(Thread.currentThread().getName()+"-->"+threadLocal.get());

        //其他线程
        new Thread(new MyRun()).start();
    }

    public static class MyRun implements Runnable{
        public void run(){
            threadLocal.set((int)(Math.random()*99));
            System.out.println(Thread.currentThread().getName()+"-->"+threadLocal.get());
        }
    }
}
