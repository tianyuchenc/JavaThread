package mythread;

import java.util.ArrayList;
import java.util.List;

/**
 * @Auther: cty
 * @Date: 2020/1/6 13:23
 * @Description: 不安全2-2
 * 不安全概述：
 * 非共享资源时，一个线程将主内存中的资源拷贝到自己的工作空间还未来得及“改”回主内存，其他线程又将该资源从主内存拷贝到自己的工作空间进行访问（读 或 改）
 * 各个线程的工作空间从主内存中读取到了重复数据进行操作（本例中是对相同的地址空间进行写入，导致容器覆盖）
 * @version: 1.0
 */
public class Hd_UnsafeTest2_2 {
    public static void main(String[] args) throws InterruptedException {
        List<String> list = new ArrayList<String>();

        for(int i=0; i<10000; i++){  //开启10000个线程
            new Thread(()->{
                list.add(Thread.currentThread().getName());
            }).start();
        }

        Thread.sleep(10000);  //防止分线程还没运行完，主线程就结束
        System.out.println(list.size());
    }
}

/*
9998
*/
