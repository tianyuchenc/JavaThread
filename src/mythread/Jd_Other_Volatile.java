package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/12 14:11
 * @Description: volatile用于保证数据的同步，也就是“可见性”
 * 线程直接访问主内存中的共享变量，而不是拷贝到工作内存
 * @version: 1.0
 */
public class Jd_Other_Volatile {
    private volatile static int num = 0;  //若不加volatile，程序会一直停留在while循环里

    public static void main(String[] args) throws InterruptedException {
        new Thread(()->{
            while(num==0){

            }
            System.out.println("线程结束！");
        }).start();

        Thread.sleep(1000);
        num = 1;
    }
}
