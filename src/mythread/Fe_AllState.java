package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 19:22
 * @Description:  查看线程的所有状态
 * @version: 1.0
 */
public class Fe_AllState {
    public static void main(String[] args) {
        //创建线程
        Thread t = new Thread(()->{
            for(int i=0; i<5; i++){
                try {
                    Thread.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                System.out.println("test");
            }
        });
        //查看状态
        Thread.State state = t.getState();
        System.out.println(state);  // new (新生)

        t.start();
        //查看状态
        state = t.getState();
        System.out.println(state);  // RUNNABLE （就绪和运行）

//        while (state != Thread.State.TERMINATED){
//            try {  //延时用
//                Thread.sleep(200);
//            } catch (InterruptedException e) {
//                e.printStackTrace();
//            }
//            state = t.getState();
//            System.out.println(state);  // RUNNABLE、TIMED_WAITING或TERMINATED
//        }

        while (true){
            int num = Thread.activeCount();
            System.out.println(num);
            if(num==2){
                break;
            }

            try {  //延时用
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            state = t.getState();
            System.out.println(state);  // RUNNABLE、TIMED_WAITING或TERMINATED
        }
    }
}
