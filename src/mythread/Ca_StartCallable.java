package mythread;

import java.util.concurrent.*;

/**
 * @Auther: cty
 * @Date: 2020/1/4 21:48
 * @Description:  了解创建线程方式三：实现Callable接口
 * 1、创建：实现Callable接口+重写call【call可以抛出异常，可以有返回值】
 * 2、启动：创建线程、创建执行服务、提交执行、获取结果、关闭服务
 * @version: 1.0
 */
public class Ca_StartCallable implements Callable<Boolean> {
    private String url;
    private String name;

    private Ca_StartCallable(String url, String name){
        this.url = url;
        this.name = name;
    }
    @Override
    public Boolean call() throws Exception{
        Ab_WebDownloader wd = new Ab_WebDownloader();
        wd.download(url, name);
        System.out.println(name);
        return true;
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        //创建线程
        Ca_StartCallable cd1 = new Ca_StartCallable("https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2801390629,897502787&fm=26&gp=0.jpg","猪猪侠.png");
        Ca_StartCallable cd2 = new Ca_StartCallable("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3462185629,2635822378&fm=26&gp=0.jpg","灰太狼.png");
        Ca_StartCallable cd3 = new Ca_StartCallable("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1371386172,2124948373&fm=26&gp=0.jpg","蜘蛛侠.png");

        //创建执行服务
        ExecutorService ser = Executors.newFixedThreadPool(3);
        //提交执行
        Future<Boolean> result1 = ser.submit(cd1);
        Future<Boolean> result2 = ser.submit(cd2);
        Future<Boolean> result3 = ser.submit(cd3);
        //获取结果
        boolean r1 = result1.get();
        boolean r2 = result2.get();
        boolean r3 = result3.get();
        //关闭服务
        ser.shutdownNow();
    }
}
