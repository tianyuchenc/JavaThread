package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/4 21:17
 * @Description:  创建线程方式二：实现Runnable接口
 * 1、创建：实现Runnable接口+重写run
 * 2、启动：创建实现类对象+Thread对象+start
 * 推荐使用方式二：
 * 1、避免单继承的局限性，有限使用接口
 * 2、方便资源共享
 * @version: 1.0
 */
public class Ba_StartRunnable implements Runnable{
    @Override
    public void run() {
        for(int i=0; i<20; i++){
            System.out.println("一边听歌");
        }
    }

    public static void main(String[] args) {
//        //创建实现类对象
//        Ba_StartRunnable sr = new Ba_StartRunnable();
//        //创建代理类对象
//        Thread t = new Thread(sr);
//        //启动
//        t.start();

        //Java中若对象只用一次，可使用匿名
        new Thread(new Ba_StartRunnable()).start();

        //主线程程序
        for(int i=0; i<20; i++){
            System.out.println("一边敲代码");
        }
    }
}

