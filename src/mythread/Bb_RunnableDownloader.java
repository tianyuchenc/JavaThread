package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/4 20:58
 * @Description: 多线程下载图片(Runnable实现)
 * @version: 1.0
 */
public class Bb_RunnableDownloader implements Runnable {
    private String url;
    private String name;

    private Bb_RunnableDownloader(String url, String name){
        this.url = url;
        this.name = name;
    }
    @Override
    public void run() {
        Ab_WebDownloader wd = new Ab_WebDownloader();
        wd.download(url, name);
        System.out.println(name);
    }

    public static void main(String[] args) {
        //创建线程
        Bb_RunnableDownloader rd1 = new Bb_RunnableDownloader("https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2801390629,897502787&fm=26&gp=0.jpg","猪猪侠.png");
        Bb_RunnableDownloader rd2 = new Bb_RunnableDownloader("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=3462185629,2635822378&fm=26&gp=0.jpg","灰太狼.png");
        Bb_RunnableDownloader rd3 = new Bb_RunnableDownloader("https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=1371386172,2124948373&fm=26&gp=0.jpg","蜘蛛侠.png");

        //启动线程
        new Thread(rd1).start();
        new Thread(rd2).start();
        new Thread(rd3).start();
    }
}
