package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/7 1:59
 * @Description: 不安全1-2
 * 不安全概述：
 *（共享资源时，一个代理线程还未来得及“改”访问的工作空间中的资源，其他代理线程又去访问（读 或 改）该资源）
 * “还未来得及”是网络延迟引起的，“其他代理线程又去访问资源”导致三个线程判断对齐和打印对齐，
 * 三个线程都会再执行一次且打印结果相同，实际一个执行就够了，导致了多余操作和重复操作。
 * @version: 1.0
 */
public class Hb_UnsafeTest1_2 implements Runnable{
    private int ticketsNum = 99;  //剩余车票数

    @Override
    public void run() {
        while(true){
            if(ticketsNum <= 0){  //【判断】
                break;
            }
            // 模拟网络延时1-导致判断对齐
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName()+"售出了 "+ticketsNum+" 号票");  //【打印】
            // 模拟网络延时2-导致打印对齐
            try {
                Thread.sleep(50);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName()+"剩余 "+--ticketsNum+"张票");  //【修改】
        }
    }

    public static void main(String[] args) {
        //一份任务
        Hb_UnsafeTest1_2 ticket = new Hb_UnsafeTest1_2();
        //多个代理
        new Thread(ticket,"窗口1").start();  //第一个new Thread开辟工作空间，其他代理线程共享工作空间中的资源
        new Thread(ticket,"窗口2").start();
        new Thread(ticket,"窗口3").start();
    }
}

/*
...
窗口3售出了 5 号票
窗口1售出了 5 号票
窗口2售出了 5 号票
窗口3剩余 4张票
窗口2剩余 2张票
窗口1剩余 3张票
窗口3售出了 2 号票
窗口2售出了 2 号票
窗口1售出了 2 号票
窗口3剩余 1张票
窗口2剩余 0张票
窗口1剩余 -1张票
窗口3售出了 -1 号票
窗口3剩余 -2张票
*/