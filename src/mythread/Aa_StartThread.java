package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/4 20:21
 * @Description: 创建线程方式一：继承Thread类
 * 1、创建：继承Thread+重写run
 * 2、启动：创建子类对象+运行start
 * @version: 1.0
 */
public class Aa_StartThread extends Thread{
    @Override
    public void run() {
        for(int i=0; i<20; i++){
            System.out.println("一边听歌");
        }
    }

    public static void main(String[] args) {
        //创建子类对象
        Aa_StartThread st = new Aa_StartThread();
        //启动线程
        st.start();  //不保证立即运行，CPU调用，应放在主程序代码片之前
//        st.run();  //普通方法调用

        //主线程程序
        for(int i=0; i<20; i++){
            System.out.println("一边敲代码");
        }
    }
}
