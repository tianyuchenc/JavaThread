package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/4 21:32
 * @Description: Runnable共享资源实例
 * @version: 1.0
 */

public class Bc_12306 implements Runnable{
    private int ticketsNum = 99;  //剩余车票数

    @Override
    public void run() {
        while(true){
            if(ticketsNum <= 0){
                break;
            }
            System.out.println(Thread.currentThread().getName()+"售出了 "+ticketsNum--+" 号票,剩余 "+ticketsNum+"张票");
        }
    }

    public static void main(String[] args) {
        //一份任务
        Bc_12306 ticket = new Bc_12306();
        //多个代理
        new Thread(ticket,"窗口1").start();  //第一个new Thread开辟工作空间，其他代理线程共享工作空间中的资源
        new Thread(ticket,"窗口2").start();
        new Thread(ticket,"窗口3").start();
    }
}

/*
...
窗口2售出了 4 号票,剩余 3张票
窗口2售出了 3 号票,剩余 2张票
窗口3售出了 12 号票,剩余 11张票
窗口2售出了 2 号票,剩余 1张票
窗口1售出了 7 号票,剩余 6张票
窗口3售出了 1 号票,剩余 0张票
*/
