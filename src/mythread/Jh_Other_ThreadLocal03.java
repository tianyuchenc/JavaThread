package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/12 18:00
 * @Description: InheritableThreadLocal:继承上下文 环境的数据 ，拷贝一份给子线程
 * @version: 1.0
 */
public class Jh_Other_ThreadLocal03 {
    private static ThreadLocal<Integer> threadLocal = new InheritableThreadLocal<>();
    public static void main(String[] args) {
        threadLocal.set(2);
        System.out.println(Thread.currentThread().getName() + "-->" + threadLocal.get());

        //线程由main线程开辟
        new Thread(() -> {
            System.out.println(Thread.currentThread().getName() + "-->" + threadLocal.get());
            threadLocal.set(200);
            System.out.println(Thread.currentThread().getName() + "-->" + threadLocal.get());
        }).start();
    }
}
