package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 19:52
 * @Description: 线程优先级 1-10  （优先级只是表示运行概率的大小）
 * 1、MIN_PRIORITY  1  最小优先级
 * 2、NORM_PRIORITY  5  默认分配优先级
 * 3、MAX_PRIORITY  10  最大优先级
 * @version: 1.0
 */
public class Ga_PriorityTest {
    static class MyPriority implements Runnable{
        @Override
        public void run() {
            System.out.println(Thread.currentThread().getName()+"-->"+Thread.currentThread().getPriority());
        }
    }

    public static void main(String[] args) {
        //查看主线程优先级
        System.out.println(Thread.currentThread().getName()+"-->"+Thread.currentThread().getPriority());

        //一份资源
        MyPriority mp = new MyPriority();
        //多份代理
        Thread t1 = new Thread(mp);
        Thread t2 = new Thread(mp);
        Thread t3 = new Thread(mp);
        Thread t4 = new Thread(mp);
        Thread t5 = new Thread(mp);
        Thread t6 = new Thread(mp);
        //设置优先级
        t1.setPriority(Thread.MAX_PRIORITY);
        t2.setPriority(Thread.MAX_PRIORITY);
        t3.setPriority(Thread.MAX_PRIORITY);
        t4.setPriority(Thread.MIN_PRIORITY);
        t5.setPriority(Thread.MIN_PRIORITY);
        t6.setPriority(Thread.MIN_PRIORITY);
        //运行
        t1.start();
        t2.start();
        t3.start();
        t4.start();
        t5.start();
        t6.start();
    }
}
