package mythread;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @Auther: cty
 * @Date: 2020/1/8 11:12
 * @Description: 线程同步-操作并发容器（内部已锁）  CopyOnWriteArrayList
 * @version: 1.0
 */
public class Hh_SynCollectTest {
    public static void main(String[] args) throws InterruptedException {
        CopyOnWriteArrayList<String> list = new CopyOnWriteArrayList<String>();

        for(int i=0; i<10000; i++){  //开启10000个线程
            new Thread(()->{
                    list.add(Thread.currentThread().getName());
            }).start();
        }

        Thread.sleep(5000);  //防止分线程还没运行完，主线程就结束
        System.out.println(list.size());
    }
}
