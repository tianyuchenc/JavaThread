package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 17:18
 * @Description: yeild方法礼让线程，直接进入就绪状态让CPU重新调度（不一定会礼让成功）
 * @version: 1.0
 */
class MyYeild implements Runnable{
    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName()+"--> start");
        if(Thread.currentThread().getName().equals("a")){
            Thread.yield();  //礼让线程
        }
        System.out.println(Thread.currentThread().getName()+"--> end");
    }
}

public class Fd_ReadyYeild{
    public static void main(String[] args) {
        MyYeild my = new MyYeild();
        new Thread(my,"a").start();
        new Thread(my,"b").start();
    }
}

/*  输出：
b--> start
a--> start
b--> end  //礼让成功！
a--> end
*/

