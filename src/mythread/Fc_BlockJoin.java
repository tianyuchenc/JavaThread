package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 17:46
 * @Description:  join方法，a线程（a的对象）在b线程内调用join，即a插队了b，b进入阻塞状态
 * @version: 1.0
 */
public class Fc_BlockJoin {
    static class Father extends Thread{
        @Override
        public void run() {
            System.out.println("一天，爸爸想抽烟，于是给儿子钱，让儿子去买烟。");
            Son son = new Son();
            son.start();
            try {
                son.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("爸爸终于等回了儿子，把儿子凶了一顿，把零钱给了儿子，然后开始吸烟。");
        }
    }

    static class Son extends Thread{
        @Override
        public void run() {
            System.out.println("儿子接过钱去买烟，路上看见一个游戏厅，于是进去玩了5个小时。");
            try {
                for(int i=1; i<=5; i++){
                    Thread.sleep(1000);
                    System.out.println(i+"个小时过去了...");
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("儿子玩完游戏，赶紧去买烟。买过烟后带着烟和零钱回家了。");
        }
    }

    public static void main(String[] args) {
        System.out.println("儿子给爸爸买烟的故事");
        Father father = new Father();
        father.start();
    }
}
