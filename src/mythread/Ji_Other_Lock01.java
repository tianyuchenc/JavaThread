package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/12 19:26
 * @Description:
 * @version: 1.0
 */

/**
 * 可重入锁: 锁可以延续使用
 *
 * @author 裴新 QQ:3401997271
 *
 */
public class Ji_Other_Lock01 {
    public void test() {
        //  第一次获得锁
        synchronized(this) {
            while(true) {
                //  第二次获得同样的锁
                synchronized(this) {
                    System.out.println("ReentrantLock!");
                }
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void main(String[] args) {
        new Ji_Other_Lock01().test();
    }

}

