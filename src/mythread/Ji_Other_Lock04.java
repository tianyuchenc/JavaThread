package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/12 19:34
 * @Description:
 * @version: 1.0
 */
import java.util.concurrent.locks.ReentrantLock;

/**
 * 可重入锁: 锁可以延续使用 + 计数器
 *
 * @author 裴新 QQ:3401997271
 *
 */
public class Ji_Other_Lock04 {
    ReentrantLock lock = new ReentrantLock();
    public void a() throws InterruptedException {
        lock.lock();
        System.out.println(lock.getHoldCount());
        doSomething();
        lock.unlock();
        System.out.println(lock.getHoldCount());
    }
    //不可重入
    public void doSomething() throws InterruptedException {
        lock.lock();
        System.out.println(lock.getHoldCount());
        //...................
        lock.unlock();
        System.out.println(lock.getHoldCount());
    }
    public static void main(String[] args) throws InterruptedException {
        Ji_Other_Lock04 test = new Ji_Other_Lock04();
        test.a();
        Thread.sleep(1000);
        System.out.println(test.lock.getHoldCount());
    }

}

