package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 20:25
 * @Description:  守护进程 （jvm不等待，若用户进程结束，程序停止）
 * @version: 1.0
 */
public class Gb_DaemonTest {
    static class God implements Runnable{
        @Override
        public void run() {
            while (true){
                System.out.println("God bless you...");
            }
        }
    }

    static class You implements Runnable{
        @Override
        public void run() {
            for(int i=0; i<365*100; i++){
                System.out.println("Happy life..."+i);
            }
        }
    }

    public static void main(String[] args) {
        God god = new God();
        You you = new You();

        Thread g = new Thread(god);
        g.setDaemon(true);  //设置为守护进程
        g.start();

        new Thread(you).start();
    }
}
