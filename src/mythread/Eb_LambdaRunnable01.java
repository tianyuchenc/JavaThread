package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 15:17
 * @Description:
 * @version: 1.0
 */

interface ILikeLambda{
    void lambda();  //若想用Lambda表达式，接口只能有一个待实现方法
}

interface ILoveLambda{
    void lambda(int n);
}

interface IInterestInLambda{
    int lambda(int a,int b);
}

public class Eb_LambdaRunnable01 {
    public static void main(String[] args) {
        //情形1：不带形参的Lambda表达式
        ILikeLambda ILike1 = ()->{
            for(int i=0; i<5; i++){
                System.out.println("I Like Lambda1!");
            }
        };
        ILike1.lambda();

        //情形2：带参数的Lambda表达式
        ILoveLambda ILove2 = (int n)->{
            for(int i=0; i<n; i++){
                System.out.println("I Love Lambda2!");
            }
        };
        ILove2.lambda(2);

        //情形3：带参数的Lambda表达式 (若只有一个形参，数据类型和小括号可省略)
//        ILoveLambda ILove3 = (n)->{  //省略数据类型
        ILoveLambda ILove3 = n->{  //省略数据类型和小括号
            for(int i=0; i<n; i++){
                System.out.println("I Love Lambda3!");
            }
        };
        ILove3.lambda(3);

        //情形4：带参数的Lambda表达式 (若方法体只有一行代码，可省略大括号)
        ILoveLambda ILove4 = n->System.out.println("I Love Lambda"+n+"!");
        ILove4.lambda(4);

        //情形5：带多个参数的Lambda表达式 (若有多个参数，数据类型可省略，小括号不可省略)
        IInterestInLambda IInterest5 = (a,b)-> {
            return a+b;  //单行带return不可省略大括号
        };
        System.out.println(IInterest5.lambda(2,3));

        //情形6：带返回值的Lambda表达式 (若要省略大括号，return也必须一同省略)
        IInterestInLambda IInterest6 = (a,b)-> a+b;
        System.out.println(IInterest5.lambda(2,4));
    }
}
