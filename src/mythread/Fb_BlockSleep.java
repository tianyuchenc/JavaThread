package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 16:51
 * @Description:  sleep模拟网络延时（放大了发生问题的可能性）
 * @version: 1.0
 */
public class Fb_BlockSleep implements Runnable{
    private int ticketsNum = 99;  //剩余车票数

    @Override
    public void run() {
        while(true){
            if(ticketsNum < 0){
                break;
            }

            //模拟延时
            try{
                Thread.sleep(200);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            System.out.println(Thread.currentThread().getName()+"-->"+ticketsNum--);
        }
    }

    public static void main(String[] args) {
        //一份资源
        Fb_BlockSleep ticket = new Fb_BlockSleep();
        //多个代理
        new Thread(ticket,"码畜").start();
        new Thread(ticket,"码农").start();
        new Thread(ticket,"码蝗").start();
    }
}
