package mythread;

/**
 * @Auther: cty
 * @Date: 2020/1/5 14:51
 * @Description: Lambda表达式简化线程（用一次、程序简单，接口只有一个方法）的使用
 * 格式：
 * (参数)->{ 方法体 }
 * 注意：lambda推导时要有类型（形参或为引用赋值）
 * @version: 1.0
 */
//外部类
class Test1 implements Runnable{
    public void run(){
        for(int i=0; i<5; i++){
            System.out.println("听歌1");
        }
    }
}

public class Ea_LambdaRunnable {
    //静态内部类
    static class Test2 implements Runnable{
        public void run(){
            for(int i=0; i<5; i++){
                System.out.println("听歌2");
            }
        }
    }

    public static void main(String[] args) {
        //局部内部类
        class Test3 implements Runnable{
            public void run(){
                for(int i=0; i<5; i++){
                    System.out.println("听歌3");
                }
            }
        }

        //方式1：使用外部类
        new Thread(new Test1()).start();
        //方式2：使用静态内部类
        new Thread(new Test2()).start();
        //方式3：使用局部内部类
        new Thread(new Test3()).start();
        //方式4：使用匿名内部类 (必须借助接口或父类)
        new Thread(new Runnable() {
            @Override
            public void run() {
                for(int i=0; i<5; i++){
                    System.out.println("听歌4");
                }
            }
        }).start();
        //方式5：使用lambda表达式 （jdk8中的简化）
        new Thread(()->{
            for(int i=0; i<5; i++){
                System.out.println("听歌5");
            }
        }).start();
    }
}
