# JavaThread

#### 介绍
Java 多线程（并发）

#### 软件架构
* 建立线程：Thread、Runnable、Callable
* 补充知识：静态代理、Lambda表达式
* 线程状态：新生态、就绪态、运行态、阻塞态、死亡态
* 线程安全：Synchronized、volatile
* 并发协作模式（线程通信模型）：生产者消费者模式（管程法和信号灯法）
* 任务定时：Timer、Quartz
* 指令重排
* 单例模式：懒汉式
* 线程本地：ThreadLocal
* 锁：可重入锁和不可重入锁
* 比较并交换：CAS


#### 运行环境

1.  jdk-13
2.  Intellij IDEA 2019.3


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
